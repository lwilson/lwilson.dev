const options: Intl.DateTimeFormatOptions = {
    dateStyle: "long"
};
export const formatter = Intl.DateTimeFormat('en-GB', options);